// import { StatusBar } from 'expo-status-bar';
// import { StyleSheet, Text, View, TextInput } from 'react-native';
// import { LinearGradient } from 'expo-linear-gradient';

// export default function App() {
//   return (
//     <View style={styles.container}>
//       <Text style={styles.titulo}>Crear cuenta</Text>
//       <TextInput
//         placeholder='Nombre de usuario'
//         style={styles.textInput}/>
//       <TextInput
//         placeholder='Email'
//         style={styles.textInput}/>
//       <TextInput
//         placeholder='Contraseña'
//         style={styles.textInput}/>
//       <StatusBar style="auto" />
//       <Text> ¿ya tienes cuenta?</Text>
//       <TouchableOpacity>      
//         <LinearGradient
//           colors={['#4c669f', '#3b5998', '#192f6a']}
//           style={styles.button}>
//           <Text style={styles.text}>Sign in with Facebook</Text>
//         </LinearGradient>
//       </TouchableOpacity>  

//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#000033',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   titulo: {
//     fontSize:30,
//     color:'white'
//   },
//   subTittle: {
//     fontSize:20,
//   },
//   textInput: {
//     fontSize:20,
//     paddingStart: 30,
//     width: '80%',
//     padding: 10,
//     marginTop:20,
//     borderRadius:20,
//     backgroundColor: '#f1f1f1',
  
//   },

//   text: {
//     fontSize:30,
//     color:'white'
//   },

// });
import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image } from 'react-native'; 
import { StatusBar } from 'expo-status-bar';
import { LinearGradient } from 'expo-linear-gradient';

export default function App() {
  return (
    <View style={styles.container}>
      <Image
        source={require('./assets/logo.png')}
        style={styles.logo}
      />
      <Text style={styles.titulo}>Crear cuenta</Text>
      <TextInput
        placeholder='Nombre de usuario'
        style={styles.textInput}/>
      <TextInput
        placeholder='Email'
        style={styles.textInput}/>
      <TextInput
        placeholder='Contraseña'
        style={styles.textInput}
        secureTextEntry={true}/>
      <StatusBar style="auto" />
      <TouchableOpacity style={styles.button}>
        <Text style={styles.text}>Continuar</Text>
      </TouchableOpacity> 
      <Text style={styles.subTittle}> ¿Ya tienes cuenta?</Text> 
      <TouchableOpacity style={styles.botoninicio}>
        <Text style={styles.textBotonInicio}>Iniciar sesión</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000033',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 200, 
    height: 200, 
    marginBottom: 5, 
  },
  titulo: {
    fontSize: 30,
    color: 'white'
  },
  subTittle: {
    marginTop: 60,
    color: 'gray'
  },
  textInput: {
    fontSize: 20,
    paddingStart: 30,
    width: '80%',
    padding: 10,
    marginTop: 20,
    borderRadius: 15,
    backgroundColor: '#f1f1f1',
  },
  button: {
    width: '80%',
    padding: 10,
    marginTop: 20,
    borderRadius: 40,
    backgroundColor: 'white', 
  },
  text: {
    fontSize: 20,
    color: '#000033',
    textAlign: 'center',
  },
  botoninicio: {
    backgroundColor: 'transparent',
    marginTop: 20,
  },
  textBotonInicio: {
    fontSize: 20,
    color: 'yellow',
    textAlign: 'center',
  }
});


